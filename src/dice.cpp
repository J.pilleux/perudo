#include <stdlib.h>

#include "dice.hpp"

namespace perudo {

    Dice::Dice() {
        roll();
    }

    void Dice::roll() {
        value = static_cast<DiceValue>(rand() % last);
    }

    DiceValue Dice::get_value() const {
        return value;
    }
}
