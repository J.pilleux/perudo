BUILD_DIR = build/
BUILD_MAKEFILE = build/Makefile

TARGET = perudo
TEST_TARGET = perudo_test

CMK = cmake
MK = make
MKFLAGS = -B$(BUILD_DIR) .
TESTFLAGS = -DTESTS=ON

.PHONY: all clean

all: $(TARGET)

test: $(TEST_TARGET)

$(TARGET):
	$(CMK) $(MKFLAGS)
	$(MK) -C $(BUILD_DIR)

$(TEST_TARGET):
	$(CMK) $(TESTFLAGS) $(MKFLAGS)
	$(MK) -C $(BUILD_DIR)
	cd $(BUILD_DIR) && ctest

clean:
	rm -rf $(BUILD_DIR) $(TARGET) $(TEST_TARGET)
