#ifndef _DICE_TEST_H_
#define _DICE_TEST_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include "dice.hpp"

class TestDice : public CppUnit::TestFixture {
    CPPUNIT_TEST_SUITE(TestDice);
    CPPUNIT_TEST(test_init);
    CPPUNIT_TEST_SUITE_END();

    public:
        void setUp(void);
        void tearDown(void);
    protected:
        void test_init(void);
    private:
        perudo::Dice* dice;
};

#endif // _DICE_TEST_H_
