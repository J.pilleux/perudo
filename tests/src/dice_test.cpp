#include <time.h>
#include <set>

#include "dice_test.hpp"

/* Overkill number of loops */
#define NB_LOOPS 1000000

using namespace std;
using namespace perudo;

CPPUNIT_TEST_SUITE_REGISTRATION(TestDice);

void TestDice::setUp(void) {
    dice = new Dice();
}

void TestDice::tearDown(void) {
    delete(dice);
}

void TestDice::test_init(void) {
    set<DiceValue> values {};
    Dice d = Dice();
    for (int i = 0; i < NB_LOOPS; ++i) {
        d.roll();
        DiceValue value = d.get_value();
        values.insert(value);
    }
    CPPUNIT_ASSERT(values.size() == 6);
}
