#include "dice_test.hpp"

#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>

int main(int argc, char** argv) {
    CPPUNIT_NS::TestResult testresult;

    CPPUNIT_NS::TestResultCollector collector;
    testresult.addListener(&collector);

    CPPUNIT_NS::BriefTestProgressListener progress;
    testresult.addListener(&progress);

    CPPUNIT_NS::TestRunner testrunner;
    testrunner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
    testrunner.run(testresult);

    CPPUNIT_NS :: CompilerOutputter outputter (&collector, std::cerr);
    outputter.write();

    return collector.wasSuccessful () ? 0 : 1;
}
