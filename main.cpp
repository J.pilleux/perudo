#include <iostream>
#include <time.h>
#include <string>

#include "dice.hpp"

using namespace std;
using namespace perudo;

int main() {
    srand(time(NULL));
    Dice d = Dice();
    DiceValue val = d.get_value();
    string msg = (val == DiceValue::paco) ? "Paco" : to_string(val + 1);
    cout << msg << endl;
    return 0;
}
