#ifndef _DICE_H_
#define _DICE_H_

namespace perudo {
    enum DiceValue { paco, two, three, four, five, six, last };

    class Dice {
        public:
            Dice();
            void roll();
            DiceValue get_value() const;

        private:
            DiceValue value;
    };
}

#endif // _DICE_H_
